import React, {useContext} from 'react';
import { useHistory } from "react-router-dom";
import { Menu } from "semantic-ui-react";
import { GlobalContext } from "../../context/Provider";
import { logout } from '../../context/actions/auth';

const Header = () => {

    const history = useHistory();
    
    const { contactsDispatch: dispatch } = useContext(GlobalContext);

    const handleLogOut = () => {
        logout(history)(dispatch);
    }

    return (
        <Menu>
            <Menu.Menu position='right'>
                <Menu.Item name='logout' onClick={handleLogOut}>
                    Log Out
                </Menu.Item>
            </Menu.Menu>
        </Menu>
    );
}

export default Header;