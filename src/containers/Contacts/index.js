import React, {useContext, useEffect} from 'react';
import { useHistory } from "react-router-dom";
import ContactsUI from '../../layout/Contacts/ContactsUI';
import { GlobalContext } from '../../context/Provider';
import getContacts from '../../context/actions/contacts/getContacts';
import deleteContact from '../../context/actions/contacts/deleteContact';
import favUnfavContact from '../../context/actions/contacts/favUnfavContact';

const ContactsContainer = () => {
    const { contactsDispatch, contactsState } = useContext(GlobalContext);
    const history = useHistory();

    const {
        contacts: { data },
    } = contactsState;
    
    const handleDeleteContact = (id) => {
        console.log(id);
        deleteContact(id)(contactsDispatch);
    };

    const handleFavUnfavContact = (id, is_favorite) => {
        favUnfavContact(id, !is_favorite)(contactsDispatch);
    };

    useEffect(() => {
        if (data.length === 0) {
          getContacts(history)(contactsDispatch);
        }
    }, []);

    return (
       <ContactsUI
        deleteContact={handleDeleteContact} 
        state={contactsState}
        favUnfavContact={handleFavUnfavContact}

       />
    );
}

export default ContactsContainer;