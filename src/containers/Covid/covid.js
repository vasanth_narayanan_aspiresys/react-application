import {useEffect, useState} from 'react';
import { fetchGlobalRecords } from '../../api/covid19';

export default () => {
    const [globalReacord, setGlobalRecord] = useState([]);
    const [coutriesRecord, setCountriesRecord] = useState([]);
    const [isLoading, setIsLoading] = useState(true);
    
    useEffect(() => {
        getCovidRecord();
    }, []);

    const getCovidRecord = async () => {
        const res = await fetchGlobalRecords();
        setGlobalRecord(res.data.Global);
        setCountriesRecord(res.data.Countries);
        setIsLoading(false);
    }

    return { globalReacord, coutriesRecord, isLoading };
}