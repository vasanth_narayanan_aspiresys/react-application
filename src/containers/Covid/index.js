import React from 'react'
import CovidUI from '../../layout/Covid/CovidUI';
import covid from './covid';

const CovidContainer = () => {
    return (
        <CovidUI covidData={covid()} />
    );
}

export default CovidContainer;