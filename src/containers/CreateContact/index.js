import React, {useState, useContext, useEffect} from 'react';
import { useHistory } from "react-router-dom";
import CreateContactUI from '../../layout/CreateContact/CreateContactUI';
import createContact from '../../context/actions/contacts/createContact';
import { GlobalContext } from '../../context/Provider';

const CreateContactContainer = () => {
    const [form, setForm] = useState({});
    const history = useHistory();

    const {
        contactsDispatch,
        contactsState: {
          addContact: { data },
        },
    } = useContext(GlobalContext);

    useEffect(() => {
        if (data) {
          history.push("/contacts");
        }
    }, [data]);

    const onChange = (e, { name, value }) => {
        setForm({ ...form, [name]: value });
        console.log(form);
    };

    const onSubmit = () => {
        createContact(form)(contactsDispatch);
    };

    return (
        <CreateContactUI 
            onSubmit={onSubmit}
            onChange={onChange}
        />
    );
}

export default CreateContactContainer;
