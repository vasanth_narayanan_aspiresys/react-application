import { useState, useContext, useEffect  } from "react";
import { GlobalContext } from "../../context/Provider";
import { login } from '../../context/actions/auth';
import { useHistory } from "react-router-dom";

export default () => {

    const history = useHistory();

    const {
        authDispatch,
        authState: {
          auth: { loading, error, data },
        },
    } = useContext(GlobalContext);

    const [form, setForm] = useState({});

    const onChange = (e, {name, value}) => {
        setForm({...form,[name]: value});
    }

    const isFormValid = form.username?.length && form.password?.length;

    const onSubmit = () => {
        login(form)(authDispatch);
    }

    useEffect(() => {
        if(data) {
            if(data.user) {
                history.push("/");
            }
        }
    }, [data]);

    return { form, onChange, isFormValid, onSubmit, loading, error};
}