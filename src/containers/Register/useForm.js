import { useState, useEffect, useContext } from "react";
import { useHistory } from "react-router-dom";
import { GlobalContext } from '../../context/Provider';
import { register } from '../../context/actions/auth';

export default () => {

    const history = useHistory();

    const {
        authDispatch,
        authState: {
          auth: { loading, error, data },
        },
    } = useContext(GlobalContext);

    const [form, setForm] = useState({});

    const isFormValid = form.username?.length && form.password?.length;

    const onChange = (e, {name, value}) => {
        setForm({...form, [name]: value});
        
    }

    const onSubmit = () => {
        register(form)(authDispatch);
    }

    useEffect(() => {
        console.log(data,"regiser");
        if (data) {
            history.push('/auth/login');
        }
    }, [data]);

    return { form, onChange, isFormValid, onSubmit, loading };
}