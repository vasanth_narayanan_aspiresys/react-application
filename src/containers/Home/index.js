import React from 'react';
import HomeUI from '../../layout/Home/HomeUI';

const HomeContainer = () => {
    return (
        <HomeUI />
    );
}

export default HomeContainer;