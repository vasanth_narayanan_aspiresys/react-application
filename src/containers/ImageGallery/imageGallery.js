import {useEffect, useState} from 'react'
import { fetchImages } from '../../api/imageGallery';

export default () => {
    const [images, setImages] = useState([]);
    const [searchInput, setsetSearchInput] = useState('mountain');

    useEffect(() => {
        getImages(searchInput);  
    }, []);

    const getImages = async (input) => {
        const res = await fetchImages(input);
        setImages(res.data.photos.photo.map(image => {
            return `https://farm${image.farm}.staticflickr.com/${image.server}/${image.id}_${image.secret}_m.jpg`;
        }));
    }

    const searchImages = () => {
        getImages(searchInput);      
    }

    const onChange = (e, { name, value }) => {
        setsetSearchInput(value);
    };
    
    return { onChange, searchImages, images, searchInput };
}
