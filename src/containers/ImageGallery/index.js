import React from 'react';
import ImageGalleryUI from '../../layout/ImageGallery/ImageGalleryUI';
import imageGallery from './imageGallery';


const ImageGalleryContainer = () => {

    return (
        <ImageGalleryUI searchData={imageGallery()} />
    );
}

export default ImageGalleryContainer;