import React from 'react';
import {
    Grid,
    Header as SemanticHeader,
    Card,
    Form,
    Button,
    Select,
} from "semantic-ui-react";
import countries from '../../utils/countries';
import HeaderUI from '../../components/Header';
import "./index.css";

const CreateContactUI = ({
    onChange,
    onSubmit
}) => {
    return (
        <div>
        <HeaderUI />
            <Grid centered>
                <Grid.Column className="form-column">
                <SemanticHeader>Create Contact</SemanticHeader>

                <Card fluid>
                    <Card.Content>
                        <Form unstackable>
                            <Form.Group widths={2}>
                                <Form.Input
                                label="First name"
                                name="firstName"
                                onChange={onChange}
                                placeholder="First name"
                                />
                                <Form.Input
                                name="lastName"
                                onChange={onChange}
                                label="Last name"
                                placeholder="Last name"
                                />
                            </Form.Group>
                            <Form.Group widths={2}>
                                <Form.Input
                                name="countryCode"
                                onChange={onChange}
                                control={Select}
                                options={countries}
                                label="Country"
                                placeholder="Country"
                                />
                                <Form.Input
                                name="phoneNumber"
                                onChange={onChange}
                                label="PhoneNumber"
                                placeholder="Phone Number"
                                />
                            </Form.Group>
                            <Form.Checkbox
                            name="isFavorite"
                            onChange={(e, data) => {
                            onChange(e, { name: "isFavorite", value: data.checked });
                            }}
                            label="Add to favorites"
                            />
                            <Button
                            // disabled={formInvalid || loading}
                            onClick={onSubmit}
                            primary
                            type="submit"
                            // loading={loading}
                            >
                            Submit
                            </Button>
                        </Form>
                    </Card.Content>
                </Card>
                </Grid.Column>
            </Grid>
        </div>
    );
}

export default CreateContactUI;