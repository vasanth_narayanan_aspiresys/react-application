import React from 'react';
import { Link } from "react-router-dom";
import { Button, Form, Grid, Header, Message, Segment } from 'semantic-ui-react'

const RegisterUI = ({form: {form, onChange, isFormValid, onSubmit, loading}}) => {
    return (
        <Grid textAlign='center' style={{ height: '100vh' }} verticalAlign='middle'>
            <Grid.Column style={{ maxWidth: 450 }}>
            <Header as='h2' color='teal' textAlign='center'>
                Register
            </Header>
            <Form size='large'>
                <Segment stacked>
                <Form.Input 
                    fluid 
                    placeholder='User Name*'
                    type='text'
                    name='username'
                    value={form.username || ''}
                    onChange={onChange}
                />
                <Form.Input 
                    fluid 
                    placeholder='First Name'
                    type='text'
                    name='firstName'
                    value={form.firstName || ''}
                    onChange={onChange}
                />
                <Form.Input 
                    fluid 
                    placeholder='Last Name'
                    type='text'
                    name='lastName'
                    value={form.lastName || ''}
                    onChange={onChange}
                />
                <Form.Input 
                    fluid 
                    placeholder='Email'
                    type='email'
                    name='email'
                    value={form.email || ''}
                    onChange={onChange}
                />
                <Form.Input
                    fluid
                    placeholder='Password*'
                    type='password'
                    name='password'
                    value={form.password || ''}
                    onChange={onChange}
                />
                <Button
                    loading={loading}
                    color='teal' 
                    fluid size='large'
                    disabled={!isFormValid}
                    onClick={onSubmit}
                >Register
                </Button>
                </Segment>
            </Form>
            <Message>
              Already have an account? <Link to='/auth/login'>Sign In</Link>
            </Message>
            </Grid.Column>
        </Grid>
    )
}

export default RegisterUI;