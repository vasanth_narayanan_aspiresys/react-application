import React from 'react';
import { Link } from "react-router-dom";
import { Button, Form, Grid, Header, Message, Segment } from 'semantic-ui-react'

const LoginUI = ({form: {form, onChange, isFormValid, onSubmit, loading, error}}) => {
    return (
        <Grid textAlign='center' style={{ height: '100vh' }} verticalAlign='middle'>
            <Grid.Column style={{ maxWidth: 450 }}>
            <Header as='h2' color='teal' textAlign='center'>
                Log-in
            </Header>
            <Form size='large'>
                <Segment stacked>
                <Form.Input 
                    fluid 
                    icon='user' 
                    iconPosition='left' 
                    placeholder='username'
                    type='text'
                    name='username'
                    value={form.username || ''}
                    onChange={onChange}
                />
                <Form.Input
                    fluid
                    icon='lock'
                    iconPosition='left'
                    placeholder='Password'
                    type='password'
                    name='password'
                    value={form.password || ''}
                    onChange={onChange}
                />
                <Button
                    loading={loading} 
                    color='teal' 
                    fluid 
                    size='large'
                    disabled={!isFormValid || loading}
                    onClick={onSubmit}
                >Login
                </Button>
                </Segment>
            </Form>
            <Message>
                New to us? <Link to='/auth/register'>Sign Up</Link>
            </Message>
            </Grid.Column>
        </Grid>
    )
}

export default LoginUI;