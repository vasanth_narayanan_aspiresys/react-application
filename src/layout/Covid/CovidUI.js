import React from 'react';
import { Container, Statistic, Card, Header, Feed } from 'semantic-ui-react'
import HeaderUI from '../../components/Header';

const CovidUI = ({covidData: {globalReacord, coutriesRecord, isLoading}}) => {
    return (
      <div>
      <HeaderUI />
        <Container text>
            <Header as='h2' textAlign='center'>Global Record</Header>
            <Statistic.Group size='tiny'>
                <Card.Group>
                    <Card>
                        <Statistic label='New Confirmed' value={globalReacord.NewConfirmed} />
                    </Card>
                    <Card>
                        <Statistic label='New Deaths' value={globalReacord.NewDeaths} />
                    </Card>
                    <Card>
                        <Statistic label='New Recovered' value={globalReacord.NewRecovered} />   
                    </Card>
                    <Card>
                        <Statistic label='Total Confirmed' value={globalReacord.TotalConfirmed} /> 
                    </Card>
                    <Card>
                        <Statistic label='Total Deaths' value={globalReacord.TotalDeaths} /> 
                    </Card>
                    <Card>
                        <Statistic label='Total Recovered' value={globalReacord.TotalRecovered} />
                    </Card>
                </Card.Group>
            </Statistic.Group>
        
            <Header as='h2' textAlign='center'>Country Records</Header>
            <Statistic.Group size='tiny'>
                {isLoading && (
                    <div className="ui active centered inline loader"></div>
                )}    
                <Card.Group>
                  {coutriesRecord.map(countryData => (
                    <Card key={countryData.Country}>
                        <Card.Content>
                            <Card.Header>{countryData.Country}</Card.Header>
                            </Card.Content>
                        <Card.Content>
                        <Feed>
                            <Feed.Event>
                            <Feed.Content>
                                <Feed.Summary>
                                    NewConfirmed: {countryData.NewConfirmed}
                                </Feed.Summary>
                                <Feed.Summary>
                                    NewDeaths: {countryData.NewDeaths}
                                </Feed.Summary>
                                <Feed.Summary>
                                    NewRecovered: {countryData.NewRecovered}
                                </Feed.Summary>
                                <Feed.Summary>
                                    TotalConfirmed: {countryData.TotalConfirmed}
                                </Feed.Summary>
                                <Feed.Summary>
                                    TotalDeaths: {countryData.TotalDeaths}
                                </Feed.Summary>
                                <Feed.Summary>
                                    TotalRecovered: {countryData.TotalRecovered}
                                </Feed.Summary>
                            </Feed.Content>
                            </Feed.Event>
                        </Feed>
                        </Card.Content>
                    </Card>
                  ))}  
                </Card.Group>
            </Statistic.Group>
        </Container>
    </div>
    );
}

export default CovidUI;