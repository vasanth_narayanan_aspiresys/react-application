import React from 'react';
import { Image, Grid, Container, Form } from 'semantic-ui-react'
import './ImageGalleryUI.css';
import HeaderUI from '../../components/Header';

const ImageGalleryUI = ({
    searchData: {onChange, searchImages, images, searchInput}
}) => {  
    return (
        <div>
        <HeaderUI />
        <Container>
            <Form className="form-container">
            <Form.Group>
                <Form.Input
                placeholder='Search'
                name='imageName'
                onChange={onChange}
                value={searchInput}
                />
                <Form.Button content='Search' onClick={searchImages} />
            </Form.Group>
            </Form>
        </Container>
        <Container>
            <Grid columns={5} divided stackable celled>
                <Grid.Row>{images.map(image => (
                    <Grid.Column key={image}>
                    <Image  src={image} />
                    </Grid.Column>
                ))}
                </Grid.Row> 
            </Grid>
        </Container>
        </div>
    );
}

export default ImageGalleryUI;