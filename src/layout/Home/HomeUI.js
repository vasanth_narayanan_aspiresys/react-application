import React from 'react';
import { Link } from 'react-router-dom';
import { Container, Grid, Button } from 'semantic-ui-react'
import HeaderUI from '../../components/Header';

const HomeUI = () => {
    return (
        <>
        <HeaderUI />
        <Container fluid>
        <Grid>
            <Grid.Column width={4}>
              <Button>
                <Link to="/contacts">
                   View Contacts
                </Link>
              </Button> 
              <Button>
                <Link to="/covid19">
                    View Covid Status
                </Link>
              </Button>
              <Button>
                <Link to="/image-gallery">
                Image Gallery
                </Link>
              </Button>  
            </Grid.Column>
        </Grid>
      </Container>
      </>
    );
}

export default HomeUI;