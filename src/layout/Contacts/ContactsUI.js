import React from 'react';
import { Link } from 'react-router-dom';
import { Grid, Button, Image, List, Icon, Placeholder, Header, Message } from 'semantic-ui-react'
import HeaderUI from '../../components/Header';

const ContactsUI = ({
    deleteContact,
    favUnfavContact,
    state: {
        contacts: { loading, isSearchActive, foundContacts, data },
    }
}) => {
    const currentContacts = isSearchActive ? foundContacts : data;
    const favtContacts = currentContacts.filter((item) => item.is_favorite);

    return (
        <>
        <HeaderUI />
        <Grid centered>
        <Grid.Column className="form-column">
        <Header as='h3' block>
        Fav Contacts
        </Header>
        <List selection verticalAlign='middle'>
        {loading && [1,2,3].map((data, index) => (
            <Placeholder key={index}>
                <Placeholder.Header image>
                <Placeholder.Line />
                <Placeholder.Line />
                </Placeholder.Header>
            </Placeholder>
            ))}
        {!loading && favtContacts.length === 0 && (
            <Message content="No Fav Contacts to show" />
        )}

        {favtContacts.length > 0 && favtContacts.map((contact, index) => (       
        <List.Item key={index}>
        <Image avatar src='https://react.semantic-ui.com/images/avatar/small/helen.jpg' />
        <List.Content>
            <List.Header>{contact.first_name} {contact.last_name}</List.Header>
        </List.Content>
        </List.Item>
        ))}
        </List>
  
            <List divided verticalAlign='middle'>
            <Header as='h3' block>
            My Contacts
            </Header>
            {loading && [1,2,3,4,5].map((data, index) => (
            <Placeholder key={index}>
                <Placeholder.Header image>
                <Placeholder.Line />
                <Placeholder.Line />
                </Placeholder.Header>
            </Placeholder>
            ))}

            <Button as={Link} to="/contacts/create" primary basic icon>
            Create Contact
            </Button>

            {!loading && currentContacts.length === 0 && (
            <Message content="No Contacts to show" />
            )}
            {currentContacts.length > 0 && currentContacts.map((contact, index) => (
                <List.Item key={index}>
                <List.Content floated='right'>
                    <Button color='twitter' onClick={() => {
                      favUnfavContact(contact.id, contact.is_favorite);
                    }}><Icon name='like' />{contact.is_favorite? "Remove Fav": "Add Fav" }</Button>
                    <Button color='youtube' onClick={() => {
                      deleteContact(contact.id);
                    }}><Icon name='user delete' />Delete</Button>
                </List.Content>
                <Image avatar src='https://react.semantic-ui.com/images/avatar/small/lena.png' />
                <List.Content>
                    <List.Header>{contact.first_name} {contact.last_name}</List.Header>
                    {contact.phone_number}
                </List.Content>
                </List.Item>
            ))}
            </List>
        </Grid.Column>
        </Grid>
        </>
    );
}

export default ContactsUI;