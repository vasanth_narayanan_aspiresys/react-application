import LoginComponent from '../containers/Login';
import RegisterComponent from '../containers/Register';
import HomeComponent from '../containers/Home';
import ImageGalleryComponent from '../containers/ImageGallery';
import CovidComponent from '../containers/Covid';
import ContactsComponent from '../containers/Contacts';
import CreateContactComponent from '../containers/CreateContact';

const routes = [
    {
        path: '/auth/login',
        component: LoginComponent,
        isNeedAuth: false
    },
    {
        path: '/auth/register',
        component: RegisterComponent,
        isNeedAuth: false
    },
    {
        path: '/image-gallery',
        component: ImageGalleryComponent,
        isNeedAuth: true
    },
    {
        path: '/covid19',
        component: CovidComponent,
        isNeedAuth: true
    },
    {
        path: "/contacts/create",
        component: CreateContactComponent,
        title: "Create Contact",
        needsAuth: true,
    },
    {
        path: '/contacts',
        component: ContactsComponent,
        isNeedAuth: true
    },
    {
        path: '/',
        component: HomeComponent,
        isNeedAuth: true
    },
];

export default routes;