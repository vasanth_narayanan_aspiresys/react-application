import axios from 'axios';
import {
    LOGIN_LOADING,
    LOGIN_SUCCESS,
    REGISTER_LOADING,
<<<<<<< HEAD
    REGISTER_SUCCESS,
    LOGOUT_USER
=======
    REGISTER_SUCCESS
>>>>>>> 8ad939fa38d261c651bdd1ab1ee53fc10f2d4a55
} from '../../context/actions/actionTypes/authActionType';

export const login = ({username, password}) => (dispatch) => {

    dispatch({
        type: LOGIN_LOADING
    });

    axios.post('https://truly-contacts.herokuapp.com/api/auth/login', {
        username,
        password
    })
    .then(res => {
        localStorage.token = res.data.token;
        dispatch({
            type: LOGIN_SUCCESS, 
            payload: res.data
        });
    });
}

export const register = ({
  email,
  password,
  username,
  lastName,
  firstName,
}) => (dispatch) => {

    dispatch({
        type: REGISTER_LOADING
    });

    axios.post('https://truly-contacts.herokuapp.com/api/auth/register', {
        email,
        password,
        username,
        first_name: lastName,
        last_name: firstName,
    })
    .then((res) => {
        dispatch({
            type: REGISTER_SUCCESS,
            payload: res.data,
        });
    });
}

export const logout = (history) => dispatch => {
    localStorage.removeItem("token");
    dispatch({
        type: LOGOUT_USER,
    });
    history.push("/auth/login");
}