import {
    ADD_REMOVE_STAR_SUCCESS,
  } from "../actionTypes/contactActionType";

 import axios from "axios";
  
  export default (id, is_favorite) => (dispatch) => {
    axios
      .patch(`https://truly-contacts.herokuapp.com/api/contacts/${id}`, { is_favorite }, {
        headers: {
            'Authorization': `Bearer ${localStorage.token}`
        }
    })
      .then((res) => {
        dispatch({
          type: ADD_REMOVE_STAR_SUCCESS,
          payload: res.data,
        });
      });
  };
  