import {
    ADD_CONTACT_SUCCESS,
  } from "../../actions/actionTypes/contactActionType";
import axios from "axios";
  
export default ({
    firstName: first_name,
    lastName: last_name,
    phoneNumber: phone_number,
    countryCode: country_code,
    isFavorite: is_favorite
}) => (dispatch) => {
    
    const body = {
        first_name,
        last_name,
        phone_number,
        country_code,
        is_favorite
    };

    axios.post("https://truly-contacts.herokuapp.com/api/contacts/",body, {
        headers: {
            'Authorization': `Bearer ${localStorage.token}`
        }
    })
    .then((res) => {
        dispatch({
            type: ADD_CONTACT_SUCCESS,
            payload: res.data,
        });
    });
};
  