import axios from 'axios';
import {
  CONTACTS_LOADING,
  CONTACTS_LOAD_SUCCESS,
} from "../../actions/actionTypes/contactActionType";

export default (history) => (dispatch) => {
    dispatch({
    type: CONTACTS_LOADING,
    });

    axios.get("https://truly-contacts.herokuapp.com/api/contacts/", {
    headers: {
        'Authorization': `Bearer ${localStorage.token}`
        }
    })
    .then((res) => {
        dispatch({
        type: CONTACTS_LOAD_SUCCESS,
        payload: res.data,
        });
    });
};
