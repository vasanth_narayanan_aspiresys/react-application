import axios from "axios";

import {
    DELETE_CONTACT_SUCCESS,
  } from "../../actions/actionTypes/contactActionType";
  
  export default (id) => (dispatch) => {
      axios.delete(`https://truly-contacts.herokuapp.com/api/contacts/${id}`, {
          headers: {
              'Authorization': `Bearer ${localStorage.token}`
          }
      })
      .then((res) => {
        dispatch({
          type: DELETE_CONTACT_SUCCESS,
          payload: id,
        });
      });
  };
  