import React, { createContext, useReducer } from 'react';
import auth from '../context/reducers/auth';
import authInitialState from './initialState/authInitialState';
import contacts from '../context/reducers/contacts';
import contactsInitialState from "../context/initialState/contactsInitialState";

export const GlobalContext = createContext({});

export const GlobalProvider = ({children}) => {

    const [authState, authDispatch] = useReducer(auth, authInitialState);
    const [contactsState, contactsDispatch] = useReducer(
      contacts,
      contactsInitialState
    );

    return (
        <GlobalContext.Provider
          value={{
            authState,
            authDispatch,
            contactsState,
            contactsDispatch,
          }}
        >
            {children}
        </GlobalContext.Provider>
    );
}