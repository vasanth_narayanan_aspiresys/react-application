import {
    LOGIN_LOADING,
    LOGIN_SUCCESS,
    REGISTER_LOADING,
    REGISTER_SUCCESS
} from '../actions/actionTypes/authActionType';

const auth = (state, {type, payload}) => {
    switch (type) {
        case REGISTER_LOADING:
        case LOGIN_LOADING:
            return {
                ...state,
                auth: {
                    ...state.auth,
                    loading: true
                }
            }
        case REGISTER_SUCCESS:    
        case LOGIN_SUCCESS:
            return {
                ...state,
                auth: {
                    ...state.auth,
                    loading: false,
                    data: payload
                }
            } 
        default:
            return state;
    }
}

export default auth;