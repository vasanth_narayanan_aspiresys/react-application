import axios from 'axios';

export const register = userData => {
    axios.post('https://truly-contacts.herokuapp.com/api/auth/register', {
        email: userData.email,
        password: userData.password,
        username: userData.username,
        last_name: userData.lastName,
        first_name: userData.firstName
    })
    .then((res) => {
        window.location = '/auth/login';
    });
}

export const login = userData => {
    axios.post('https://truly-contacts.herokuapp.com/api/auth/login', {
        username: userData.username,
        password: userData.password
    })
    .then(res => {
        localStorage.setItem("token", res.data.token);
    });
}