import axios from 'axios';

export const fetchGlobalRecords = () => {
    return axios.get('https://api.covid19api.com/summary');
}